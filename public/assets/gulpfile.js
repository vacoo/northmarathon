var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

var paths = {
    css: [
        'css/*/*.scss'
    ],
    js: [
        'js/src/photoswipe.js',
        'js/src/countdown.js',
        'js/src/postmessage.js'
    ],
    cssLibs: [
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/photoswipe/dist/photoswipe.css',
        'node_modules/photoswipe/dist/default-skin/default-skin.css',
    ],
    jsLibs: [
        // 'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/photoswipe/dist/photoswipe.js',
        'node_modules/photoswipe/dist/photoswipe-ui-default.js',
    ],
    imagesLibs: [
        'node_modules/photoswipe/dist/default-skin/default-skin.png',
        'node_modules/photoswipe/dist/default-skin/default-skin.svg',
        'node_modules/photoswipe/dist/default-skin/preloader.png',
    ]
}
// SASS -> CSS -> CSS MINIFY
gulp.task('compileSASS', function(){
    return gulp.src('css/app.scss')

      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('css'))

      .pipe(minifyCss())
      .pipe(rename({
          suffix: ".min",
      }))
      .pipe(gulp.dest('css'));
});

// Обработка JS
gulp.task('compileJs', function(){
    return gulp.src(paths.js)

        .pipe(concat('app.js'))
        .pipe(gulp.dest('js'))

        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('js'));
});

// Обработка CSS библиотек
gulp.task('compileCssLibs', function(){
    return gulp.src(paths.cssLibs)
        .pipe(gulp.dest('css/vendors'))

        .pipe(concat('libs.css'))
        .pipe(gulp.dest('css'))

        .pipe(minifyCss())
        .pipe(rename({
            suffix: ".min",
        }))
        .pipe(gulp.dest('css'));
});

// Обработка JS библиотек
gulp.task('compileJsLibs', function(){
    return gulp.src(paths.jsLibs)
        .pipe(gulp.dest('js/vendors'))

        .pipe(concat('libs.js'))
        .pipe(gulp.dest('js'))

        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('js'));
});
gulp.task('imagesLibs', function(){
    return gulp.src(paths.imagesLibs)
        .pipe(gulp.dest('images'));
})
// Слежение за изменениями в scss файлах
gulp.watch(paths.css, ['compileSASS']);
gulp.watch(paths.js, ['compileJs']);

// Задача по умолчанию
gulp.task('default', ['compileSASS', 'compileJs']);
gulp.task('compileLibs', ['compileCssLibs', 'compileJsLibs', 'imagesLibs'])
